import React from 'react';

import {Link} from 'react-router-dom';

class GameOver extends React.Component {
    render(){
        return(
            <div>
                <h1>Game over! Friendship is win!</h1>
                <Link to='/'>Вернуться на главную</Link>
            </div>
        );
    }
}

export default GameOver;