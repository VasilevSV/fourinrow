import React from 'react';
import './App.css';
import StartScreen from './startScreen';
import Game from './game';
import GameOver from './gameover';
import NotFound from './404';
import {HashRouter, Route, Switch} from "react-router-dom";

class App extends React.Component {
    render() {
        return (
            <HashRouter>
                <Switch>
                    <Route path='/' exact component={StartScreen} />
                    <Route path='/game' component={Game} />
                    <Route path='/gameover'component={GameOver} />
                    <Route component={NotFound} />
                </Switch>
            </HashRouter>
        )}
}

export default App;
