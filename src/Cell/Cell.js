import  React from 'react'

export class Cell extends React.Component {
    constructor() {
        super();

        this.onClick = this.onClick.bind(this);
    }

    onClick() {
        this.props.onClick(this.props.idx);
        this.setClass();
    }

    setClass() {
        console.log(this.props.value);
    }

    render() {
        return <div className={`cell`} onClick={this.onClick}>{this.props.value}</div>
    }
}