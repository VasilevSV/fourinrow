import React from 'react';
import {Link} from 'react-router-dom';

class StartScreen extends React.Component {
    constructor(){
        super();
        this.state = {
            player1:"",
            player2:""
        }
        this.onChangePlayer1 = this.onChangePlayer1.bind(this);
        this.onChangePlayer2 = this.onChangePlayer2.bind(this);
    }

    onChangePlayer1(event){
        this.setState({
            player1: event.target.value
        });
    }
    onChangePlayer2(event){
        this.setState({
            player2: event.target.value
        });
    }

    render(){
        return(
            <div>
                <h1>Welcome</h1>
                <p>Игрок 1:<input type = "text" value={this.state.player1} onChange={this.onChangePlayer1}/></p>
                <p>Игрок 2:<input type = "text" value={this.state.player2} onChange={this.onChangePlayer2}/></p>
                <Link to={{
                    pathname:'/game',
                    state:{
                        fromStartPage: true,
                        player1: this.state.player1,
                        player2: this.state.player2
                    }
                }}>Начать игру</Link>
            </div>
        );
    }
}

export default StartScreen;