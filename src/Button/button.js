import React from 'react';

export class Button extends React.Component {
    constructor() {
        super();

        this.onButtonClick = this.onButtonClick.bind(this);
    }

    onButtonClick() {
        this.props.onClick(this.props.idx);
    }

    render () {
        return <button onClick={this.onButtonClick}>{this.props.idx + 1}</button>
    }
};
