import React from 'react';
import {Column} from './Column/Column';
import {Button} from "./Button/button";
import {Redirect, Link} from 'react-router-dom';
import './App.css'

class Game extends React.Component {
    constructor() {
        super();

        this.state = {
            field: [
                [0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0]
            ],

            currentPlayer: 1
        };

        this.makeMove = this.makeMove.bind(this);
        this.makeTurn = this.makeTurn.bind(this);
        this.newCheckHorizontals = this.newCheckHorizontals.bind(this);
        this.newCheckVerticals = this.newCheckVerticals.bind(this);
        this.checkDiagonals = this.checkDiagonals.bind(this)
    }

    // TODO remove
    makeMove(column, cell) {
        const newField = this.state.field;
        const player = this.state.currentPlayer;

        newField[column][cell] = player;

        const newPlayer = this.state.currentPlayer === 1 ? 2 : 1;

        this.setState({
            field: newField,
            currentPlayer: newPlayer
        })
    }

    newCheckHorizontals = (line) => {
        const {currentPlayer, field} = this.state;

        for (let i = 0; i <= 3; i++) {
            if (field[i][line] === currentPlayer
                && field[i+1][line] === currentPlayer
                && field[i+2][line] === currentPlayer
                && field[i+3][line] === currentPlayer) {
                alert('gameH');
                break;
            }
        }
    }

    newCheckVerticals = (column) => {
        const {currentPlayer, field} = this.state;

        for (let i = 0; i <= 3; i++) {
            if (field[column][i] === currentPlayer
                && field[column][i+1] === currentPlayer
                && field[column][i+2] === currentPlayer
                && field[column][i+3] === currentPlayer) {
                alert('gameV');
                break;
            }
        }
    }

    isExists = (value) => value >= 0 && value < 7;

    checkDiagonals = (column, line) => {
        const {currentPlayer, field} = this.state;

        if (this.isExists(column+1) && this.isExists(line-1) && field[column+1][line-1] === currentPlayer &&
            this.isExists(column+2) && this.isExists(line-2) && field[column+2][line-2] === currentPlayer &&
            this.isExists(column+3) && this.isExists(line-3) && field[column+3][line-3] === currentPlayer) {
            alert('game')
        } else if (this.isExists(column-1) && this.isExists(line+1) && field[column-1][line+1] === currentPlayer &&
            this.isExists(column+1) && this.isExists(line-1) && field[column+1][line-1] === currentPlayer &&
            this.isExists(column+2) && this.isExists(line-2) && field[column+2][line-2] === currentPlayer) {
            alert('game')
        } else if (this.isExists(column-2) && this.isExists(line+2) && field[column-2][line+2] === currentPlayer &&
            this.isExists(column-1) && this.isExists(line+1) && field[column-1][line+1] === currentPlayer &&
            this.isExists(column+1) && this.isExists(line-1) && field[column+1][line-1] === currentPlayer) {
            alert('game')
        } else if (this.isExists(column-3) && this.isExists(line+3) && field[column-3][line+3] === currentPlayer &&
            this.isExists(column-2) && this.isExists(line+2) && field[column-2][line+2] === currentPlayer &&
            this.isExists(column-1) && this.isExists(line+1) && field[column-1][line+1] === currentPlayer) {
            alert('game')

        } else if (this.isExists(column+1) && this.isExists(line+1) && field[column+1][line+1] === currentPlayer &&
            this.isExists(column+2) && this.isExists(line+2) && field[column+2][line+2] === currentPlayer &&
            this.isExists(column+3) && this.isExists(line+3) && field[column+3][line+3] === currentPlayer) {
            alert('game')
        } else if (this.isExists(column-1) && this.isExists(line-1) && field[column-1][line-1] === currentPlayer &&
            this.isExists(column+1) && this.isExists(line+1) && field[column+1][line+1] === currentPlayer &&
            this.isExists(column+2) && this.isExists(line+2) && field[column+2][line+2] === currentPlayer) {
            alert('game')
        } else if (this.isExists(column-2) && this.isExists(line-2) && field[column-2][line-2] === currentPlayer &&
            this.isExists(column-1) && this.isExists(line-1) && field[column-1][line-1] === currentPlayer &&
            this.isExists(column+1) && this.isExists(line+1) && field[column+1][line+1] === currentPlayer) {
            alert('game')
        } else if (this.isExists(column-3) && this.isExists(line-3) && field[column-3][line-3] === currentPlayer &&
            this.isExists(column-2) && this.isExists(line-2) && field[column-2][line-2] === currentPlayer &&
            this.isExists(column-1) && this.isExists(line-1) && field[column-1][line-1] === currentPlayer) {
            alert('game')
        }
    }

    newCheckDiagonals = (column, line) => {
        const {currentPlayer, field} = this.state;

        for (let i=-3; i<=1; i++) {
            for (let j=-3; j<=1; j++) {

             if  (this.isExists(i) && this.isExists(j) && field[i][j] === currentPlayer &&
                this.isExists(i+1) && this.isExists(j+1) && field[i+1][j+1] === currentPlayer &&
                this.isExists(i+2) && this.isExists(j+2) && field[i+2][j+2] === currentPlayer &&
                this.isExists(i+3) && this.isExists(j+3) && field[i+3][j+3] === currentPlayer) {
                 alert('gameD');
                 break;
                }
            }
        }

    }

    makeTurn(column) {
        const {field: newField} = this.state;
        const player = this.state.currentPlayer;

        for (let i = 6; i >= 0; i--) {
            if (this.state.field[column][i] === 0) {
                newField[column][i] = player;
                this.newCheckHorizontals(i);
                this.newCheckVerticals(column);
                this.checkDiagonals(column, i);
                break;
            }
        }


        const newPlayer = this.state.currentPlayer === 1 ? 2 : 1;

        this.setState({
            field: newField,
            currentPlayer: newPlayer
        })
    }

    render() {

        const fromStP = this.props.location.state && this.props.location.state.fromStartPage;

        const player1 = this.props.location.state ? this.props.location.state.player1 : <Redirect to= '/' />;
        const player2 = this.props.location.state ? this.props.location.state.player2 : <Redirect to= '/' />;

        return (

            <>
                {!fromStP && <Redirect to= '/' />}
                <div>Игрок 1: {player1}</div>
                <div>Игрок 2: {player2}</div>

                <div className="gameField">
                    <div className="controls">

                        {this.state.field.map((el, i) => {
                            return <Button idx={i} onClick={this.makeTurn} onPress={this.checkWin} key={i}/>
                        })}
                    </div>
                    <div className="table">

                        {this.state.field.map((el, i) => {
                            return <Column values={el} idx={i} onClick={this.makeMove} key={i}/>
                        })}
                    </div>
                </div>

                <div><Link to='/gameover'>Закончить</Link></div>
            </>
        );
    }

}

export default Game;
