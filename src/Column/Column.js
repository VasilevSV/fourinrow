import React from 'react';
import {Cell} from "../Cell/Cell";

 export class Column extends React.Component {
     constructor() {
         super();

         this.onCellClick = this.onCellClick.bind(this);
     }

     onCellClick(cellIndex) {
         this.props.onClick(this.props.idx, cellIndex);
     }

     showCells(values) {
         return values.map((el, i) => {
             return <Cell onClick={this.onCellClick} value={el} idx={i} key={i}/>
         })
     }

    render () {
       return <div className="column">{this.showCells(this.props.values)}</div>
    }
};
